import type StockBuyList from "./StockBuyList";

export default interface StockBuy {
    id?: number;
    name?: string;
    store?: string;
    amount?: number;
    unit?: string;
    price?: number;
    totalPrice?: number;
    stockBuyList?: StockBuyList;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}