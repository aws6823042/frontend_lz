import type Menu from "./Menu";

export default interface Basket {
  id?: number;
  amount: number;
  note: string;
  order: string;
  menu?: Menu;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}