import type User from "./User";

export default interface Check {

  id: number;


   user?: User;

   userId?: number;


  isLate: boolean;


  checkIn: String;  

  checkOut: String;  


  createdAt?: Date;


  updatedAt?: Date;


  deletedAt?: Date;

}