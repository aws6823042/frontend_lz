import type Order from "./Order";
import type User from "./User";

export default interface Payment {
  id?: number;
  price: number;
  received: number;
  moneyChange: number;
  netPrice: number;
  discount: number;
  user_id?: User;
  order_id?: Order;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}