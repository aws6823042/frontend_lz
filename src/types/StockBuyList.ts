import type User from "./User";

export default interface StockBuyList {
    id?: number;
    user?: User;
    totalPrice?: number;
    // store?: string;
    createdAt?: Date;
    updatedAt?: Date;
    deletedAt?: Date;
}