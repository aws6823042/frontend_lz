import type Menu from "./Menu";
import type Order from "./Order";

export default interface Orderitem {
  id?: number;
  amount: number;
  note: string;
  // status: string;
  numCancel: number;
  numConfirm: number;
  order: Order;
  menu: Menu;
  createdAt?: Date;
  updatedAt?: Date;
  deletedAt?: Date;
}