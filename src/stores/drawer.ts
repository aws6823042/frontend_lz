import { ref, computed } from 'vue'
import { defineStore } from 'pinia'

export const useDrawerStore = defineStore('drawer', () => {
    const drawer = ref(false);
    function showDrawer() {
        if (drawer.value == true) {
          drawer.value = false;
        } else {
          drawer.value = true;
        }
      }

  return { showDrawer,drawer  }
})
