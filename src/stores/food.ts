import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type Menu from "@/types/Menu";
import type Option from "@/types/Option";
import basketService from "@/services/basket";
import orderitemService from "@/services/orderitem"
import queueService from "@/services/queue"
import orderService from '@/services/order'
import type Basket from '@/types/Basket';
import type BasketAdd from '@/types/BasketAdd';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';
import type Orderitem from '@/types/Orderitem';
import type Order from '@/types/Order';
import type OrderitemAdd from '@/types/OrderitemAdd';
import type QueueAdd from '@/types/QueueAdd';


export const useFoodStore = defineStore('food', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const order = ref('onOrder');
  const uuid = ref('');
  const sum = ref(0);
  const deleteDialog =ref(false);
  const Amount = ref(1);
  const Note = ref('');
  const table = ref('??');
  const baskets = ref<Basket[]>([]);
  const Dbaset = ref<Basket>();
  const orderitem = ref<Order>();
  const basket = ref<Basket>({ amount: 0 ,note: "", order: ""});
  const editedbasket = ref<BasketAdd>({ amount: 0 ,note: "", order: "", menu: 0});
  const menuiItem = ref<Menu>({ id: 0 ,name: "", price: 0 ,type: "",status: "on",image: "" });
  const confirmorder = ref<OrderitemAdd>({ amount: 0 ,note: "", order: "", menu: 0});
  const confirmList = ref<Orderitem[]>([]);
  const queueAdd = ref<QueueAdd>({status: "",orderitem: 0})
  const newIdOrderItem = ref(0);
  const options = ref<Option[]>([
    {
      id: 1,
      Optionname: "ทั้งหมด",
    },
    {
      id: 2,
      Optionname: "อาหารจานเดียว",
    },
    {
      id: 3,
      Optionname: "ยำ",
    },
    {
      id: 4,
      Optionname: "กับข้าว",
    },
    {
      id: 5,
      Optionname: "ของทานเล่น",
    },
    {
      id: 6,
      Optionname: "เครื่องดื่ม",
    },
  ]);
  function clear(){
    queueAdd.value = ({status: "",orderitem: 0})
    menuiItem.value = ({ id: 0 ,name: "", price: 0 ,type: "",status: "on",image: ""})
    editedbasket.value = ({ amount: 0 ,note: "", order: "", menu: 0})
    basket.value = ({ amount: 0 ,note: "", order: ""});
    confirmorder.value = ({ amount: 0 ,note: "", order: "", menu: 0});
    Amount.value = 1
    Note.value = ''
    newIdOrderItem.value = 0
  }

  async function getOrderItembyId(id: number) {
    try {
      const res = await orderService.getOrderbyid(id);
      orderitem.value = res.data;
      if(orderitem.value?.status != 'กำลังใช้งาน'){
        messageStore.showError("Order นี้ไม่ได้ถูกใช้งาน หรือ ชำระเงินแล้ว");
      }
    } catch (e) {
      console.log(e);
    }
    table.value = orderitem.value?.table_id.type!+orderitem.value?.table_id.Number;
  }

  async function getOrderbyUuid(uuid: string) {
    try {
      const res = await orderService.getOrderByUuid(uuid);
      orderitem.value = res.data;
      if(orderitem.value?.status != 'กำลังใช้งาน'){
        messageStore.showError("Order นี้ไม่ได้ถูกใช้งาน หรือ ชำระเงินแล้ว");
      }
    } catch (e) {
      console.log(e);
    }
    order.value = ''+orderitem.value?.id;
    table.value = orderitem.value?.table_id.type!+orderitem.value?.table_id.Number;
  }




  async function saveOrderlist() {
    loadingStore.isLoading = true;
    try {
      if (confirmorder.value.id) {
        const res = await orderitemService.updateOrderItem(
          confirmorder.value.id,
          confirmorder.value
        );
      } else {  
        const res = await orderitemService.saveOrderItem(confirmorder.value);
        newIdOrderItem.value = res.data.id;
        console.log(basket.value.menu?.type)
        if(basket.value.menu?.type == 'เครื่องดื่ม'){
          queueAdd.value.status = 'กำลังรอเสิร์ฟ';
        }else{
          queueAdd.value.status = 'กำลังรอพ่อครัว';
        }
        queueAdd.value.orderitem = newIdOrderItem.value;
        for(let i = 0; i < confirmorder.value.amount; i++){
          saveQueue()
        }
      }
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function saveQueue() {
    loadingStore.isLoading = true;
    try {
      if (queueAdd.value.id) {
        const res = await queueService.updateQueue(
          queueAdd.value.id,
          queueAdd.value
        );
      } else {  
        const res = await queueService.saveQueue(queueAdd.value);
      }
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึกได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function confirmOrder(order:string, id: number){
    await clear()
    basket.value = (await basketService.getBasketByID(order,id)).data;
    confirmorder.value.amount = basket.value.amount;
    confirmorder.value.note = basket.value.note;
    // if(basket.value.menu?.type == 'เครื่องดื่ม'){
    //   confirmorder.value.status = "กำลังรอเสิร์ฟ";
    // }else{
    //   confirmorder.value.status = "กำลังรอพ่อครัว";
    // }
    confirmorder.value.order = basket.value.order!;
    confirmorder.value.menu = basket.value.menu!.id!;
    await saveOrderlist()
    await deleteBasket(basket.value.id!)
  }

  function sumprice(){
    sum.value = 0;
    for(const item of confirmList.value){
      sum.value += (item.amount - item.numCancel) * item.menu.price
      table.value = item.order.table_id.type+item.order.table_id.Number;
      
    }  
  }

  async function getOrderCF(order:string) {
      try {
        const res = await orderitemService.getBasketByOrder(order);
        confirmList.value = res.data;
      } catch (e) {
        console.log(e);
      }
      sumprice();
  }


  async function getBaskets(order:string) {
    if(order != null){
      try {
        const res = await basketService.getBasketByOrder(order);
        baskets.value = res.data;
      } catch (e) {
        console.log(e);
        messageStore.showError("กรุณาสแกนผ่าน QR code");
      }
    }
  }

  async function basketEditItem(order:string,id: number) {
    basket.value = (await basketService.getBasketByID(order,id)).data;
    Amount.value = basket.value.amount
    Note.value = basket.value.note
    order = basket.value.order
    editedbasket.value.id = basket.value.id;
    menuiItem.value = basket.value.menu!;
    dialog.value = true;
  }

  async function saveBasket() {
    loadingStore.isLoading = true;
    try {
      if (editedbasket.value.id) {
        const res = await basketService.updateBasket(
          editedbasket.value.id,
          editedbasket.value
        );
      } else {
        const res = await basketService.saveBasket(editedbasket.value);
      }
      dialog.value = false;
      await getBaskets(order.value);
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก Menu ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteBasket(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await basketService.deleteBasket(order.value,id);
      await getBaskets(order.value);
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูลได้");
      console.log(e);
    }
    deleteDialog.value = false;
    loadingStore.isLoading = false;
  }


  return { getOrderbyUuid,uuid,saveQueue,queueAdd,getOrderItembyId,orderitem,Dbaset,deleteDialog,table,sum,options,dialog,menuiItem,Amount,Note,clear,order,getBaskets,baskets,saveBasket,editedbasket,deleteBasket,basketEditItem,confirmOrder,getOrderCF,confirmList,confirmorder,saveOrderlist }
})
