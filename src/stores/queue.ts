import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import queueService from "@/services/queue"
import type Orderitem from '@/types/Orderitem';
import { useLoadingStore } from './loading';
import { useMessageStore } from './message';
import type Queue from '@/types/Queue';
import type Menu from '@/types/Menu';

export const useQueueStore = defineStore('queue', () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const cancelDialog = ref(false);
  const queues = ref<Queue[]>([]);
  const cooks = ref<Queue[]>([]);
  const serve = ref<Queue[]>([]);
  const cancelMenu = ref();

  async function getOrderToQueue(text:string) {
      try {
        const res = await queueService.getQueueItemByStatus(text);
        return res.data;
      } catch (e) {
        console.log(e);
        messageStore.showError("ไม่สามารถดึงข้อมูลได้");
      }
  }

  function dialogCancel(item: Queue){
    // console.log(item)
    cancelMenu.value = item.orderitem.menu
    cancelDialog.value = true
  }

  return { getOrderToQueue, queues, cooks, cancelDialog, dialogCancel ,serve ,cancelMenu}
})
