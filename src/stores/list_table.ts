import { ref, watch } from "vue";
import { defineStore } from "pinia";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";
import type Table from "@/types/ListTable";
import tableService from "@/services/list_table";

export const useTableListStore = defineStore("TableList", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const deleteDialog = ref(false);
  const dID = ref();
  const dType = ref();
  const dNumber = ref();
  const tables = ref<Table[]>([]);
  const editedTable = ref<Table>({ type: "", Number: 0, status: "" });
  const search = ref("");

  function dialogDelete(item: Table) {
    dID.value = item.id
    dType.value = item.type;
    dNumber.value = item.Number;
    deleteDialog.value = true
  }

  watch(dialog, (newDialog, oldDialog) => {
    console.log(newDialog);
    if (!newDialog) {
      editedTable.value = { type: "", Number: 0, status: "" };
    }
  });
  async function getTable() {
    try {
      if (search.value == '') {
        const res = await tableService.getTable();
        tables.value = res.data;
      } else {
        const res = await tableService.getTableByName(search.value);
        tables.value = res.data;
      }
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล List Table ได้");
    }
  }

  async function getTablesByName(name: string) {
    try {
      const res = await tableService.getTableByName(name);
      tables.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล List Table ได้");
    }
  }

  async function saveTable() {
    loadingStore.isLoading = true;
    try {
      if (editedTable.value.id) {
        const res = await tableService.updateTable(
          editedTable.value.id,
          editedTable.value
        );
      }
      else {
        const res = await tableService.saveTable(editedTable.value);
      }
      dialog.value = false;
      await getTable();
    } catch (e) {
      messageStore.showError("ไม่สามารถบันทึก List Table ได้");
      console.log(e);
    }
    loadingStore.isLoading = false;
  }

  async function deleteTable(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await tableService.deleteTable(id);
      await getTable();
    } catch (e) {
      messageStore.showError("ไม่สามารถลบข้อมูล List Table ได้");
      console.log(e);
    }
    deleteDialog.value = false;
    loadingStore.isLoading = false;
  }

  function editTable(table: Table) {
    editedTable.value = JSON.parse(JSON.stringify(table));
    dialog.value = true;
  }
  return { tables, getTable, dialog, editedTable, saveTable, deleteTable, editTable, deleteDialog, dialogDelete, dNumber, dType, dID, getTablesByName, search };
});