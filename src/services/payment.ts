import type Payment from "@/types/Payment";
import type PaymentAdd from "@/types/PaymentAdd";
import http from "./axios";

function getPayment(){
  return http.get("/payment");
}
function savePayment(payment: PaymentAdd){
  return http.post("/payment", payment);
}
function updatePayment(id: number, payment: Payment){
  return http.patch(`/payment/${id}`, payment);
}
function deletePayment(id: number){
  return http.delete(`/payment/${id}`);
}
export default {getPayment, savePayment, updatePayment, deletePayment}