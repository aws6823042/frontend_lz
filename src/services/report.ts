import http from "./axios";

  function CookBy(){
    return http.get("/reports/CookDay")
  }

  function sumPrice(text:string){
    return http.get(`/reports/price?search=${text}`)
  }

  function sumOrder(text:string){
    return http.get(`/reports/order?search=${text}`)
  }

  function countTable(text:string){
    return http.get(`/reports/table?search=${text}`)
  }

  function topMenu(text:string){
    return http.get(`/reports/topMenu?search=${text}`)
  }

  function topChef(text:string){
    return http.get(`/reports/topChef?search=${text}`)
  }

  function topServe(text:string){
    return http.get(`/reports/topServe?search=${text}`)
  }

  function price7day(){
    return http.get(`/reports/price7day`)
  }

  function price12month(){
    return http.get(`/reports/price12month`)
  }

  

export default {CookBy,sumPrice,sumOrder,countTable,topMenu,topChef,topServe,price7day,price12month}