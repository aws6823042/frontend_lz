import type Stock from "@/types/Stock";
import http from "./axios";

function getStock(){
  return http.get("/stocks");
}
function getStockByName(name:string){
  return http.get(`/stocks/name/${name}`);
}
function saveStock(stock: Stock){
  return http.post("/stocks", stock);
}
function updateStock(id: number, stock: Stock){
  return http.patch(`/stocks/${id}`, stock);
}
function deleteStock(id: number){
  return http.delete(`/stocks/${id}`);
}
export default {getStock, saveStock, updateStock, deleteStock, getStockByName};