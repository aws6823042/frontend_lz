
import type QueueAdd from "@/types/QueueAdd";
import http from "./axios";

// function getQueue(){
//   return http.get("/tables");
// }
function saveQueue(queue: QueueAdd){
  return http.post("/queues", queue);
}

function getQueueItemByStatus(text: string){
  return http.get(`/queues/status/${text}`);
}
function updateQueue(id: number, queue: QueueAdd){
  return http.patch(`/queues/${id}`, queue);
}
// function deleteTable(id: number){
//   return http.delete(`/tables/${id}`);
// }
export default {saveQueue,getQueueItemByStatus,updateQueue};