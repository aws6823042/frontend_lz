import type Menu from "@/types/Menu";
import http from "./axios";
function getMenus() {
  return http.get("/menus");
}

function getMenuByName(name:string){
  return http.get(`/menus/name/${name}`);
}

function saveMenu(menu: Menu & {files: File[]}) {
  const formDate = new FormData();
  formDate.append("name",menu.name)
  formDate.append("price",`${menu.price}`)
  formDate.append("type",menu.type)
  formDate.append("status",menu.status)
  if(menu.files){
  formDate.append("file",menu.files[0])
  }
  return http.post("/menus", formDate, {headers: {
    "Content-Type": "multipart/form-data",
  }});
}

function updateMenu(id: number, menu: Menu & {files: File[]}) {
  const formDate = new FormData();
  formDate.append("name",menu.name)
  formDate.append("price",`${menu.price}`)
  formDate.append("type",menu.type)
  formDate.append("status",menu.status)
  if(menu.files){
    formDate.append("file",menu.files[0])
  }
  return http.patch(`/menus/${id}`, formDate, {headers: {
    "Content-Type": "multipart/form-data",
  }});
}

function deleteMenu(id: number) {
  return http.delete(`/menus/${id}`);
}

export default { getMenus, saveMenu, updateMenu, deleteMenu,getMenuByName };
