import type Table from "@/types/ListTable";
import http from "./axios";

function getTable(){
  return http.get("/tables");
}
function getTableByName(name:string){
  return http.get(`/tables/name/${name}`);
}
function saveTable(table: Table){
  return http.post("/tables", table);
}
function updateTable(id: number, table: Table){
  return http.patch(`/tables/${id}`, table);
}
function deleteTable(id: number){
  return http.delete(`/tables/${id}`);
}
export default {getTable, saveTable, updateTable, deleteTable, getTableByName};