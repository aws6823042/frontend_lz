import type Serve from "@/types/Serve";
import http from "./axios";
function getServes() {
  return http.get("/serve");
}
function saveServe(serve: Serve) {
  return http.post("/serve", serve);
}
function updateServe(id: number, serve: Serve) {
  return http.patch(`/serve/${id}`, serve);
}
function deleteServe(id: number) {
  return http.delete(`/serve/${id}`);
}

export default { getServes ,deleteServe,updateServe,saveServe };
